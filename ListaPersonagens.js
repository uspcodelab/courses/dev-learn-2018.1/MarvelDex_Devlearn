	/* let heroes = ["Ant-Man (Eric O'Grady)", "Black Panther","Black Widow","Captain America",
				"Captain Marvel (Carol Danvers)", "Daredevil","Doctor Strange","Drax","Falcon",
				"Gambit","Gamora","Groot","Hawkeye","Hulk","Iron Man","Loki",
				"Magneto","Mantis","Nebula","Odin","Red Skull","Rocket Raccoon","Scarlet Witch",
				"Spider-Man","Star-Lord (Peter Quill)","Thanos","Thor",
				"Ultron","Vision","War Machine (Iron Man 3 - The Official Game)",
				"Wasp","Winter Soldier","Wolverine"];
	*/

	let heroesId = ["1009187", "1009189", "1010802", "1009220", "1009262", "1010338", "1009282", "1009297", "1010735", "1009313", "1010743", 
				"1010763", "1009338", "1009407", "1009351", "1009368", "1011026", "1009417", "1010365", "1009480", "1009535", "1010744", 
				"1009562", "1009610", "1009652", "1010733", "1009664", "1009685", "1009697", "1017322", "1010740", "1009707", "1009718"]

				var modal = document.getElementById('myModal');

	
	heroesId.map((heroId) => {
		link =`http://gateway.marvel.com/v1/public/characters/${heroId}?&ts=1&offset=0&apikey=672cfdade3ba8021ac86f3cdb83dc04a&hash=6dd2042ceb19714921e65dc754d0cb0f`
		axios.get(link)
		  .then(
		  	(response) => {

		  		
		  		let content = document.getElementsByClassName("personagens-container");


		  		let a = document.createElement('a');
				let conteudo = document.getElementById("conteudoModal")

				let json = response.data;
				
		  		let thumbnail = json.data.results[0].thumbnail;


				let id = json.data.results[0].id;
				  
				

		  		//a.href=`InfoPersonagem.html?id=${id}`;

				sessionStorage.setItem(`${id}`, JSON.stringify(json));
				  
				a.onclick = function() 
				{
					modal.style.display = "block";
					// let n = conteudo.childNodes.length-1
					// console.log(n)
					// if(n>=2){
					// 	conteudo.remove(conteudo.lastChild)
					// 	console.log('entrei no if')
					// }

					// console.log('sai do if')
					let infoText = sessionStorage.getItem(id);
					let Pjson = JSON.parse(infoText)

					let Pthumbnail = Pjson.data.results[0].thumbnail;
					let Pname = Pjson.data.results[0].name;
					let Pdesc = Pjson.data.results[0].description;

					imagemURL = `${Pthumbnail.path}/portrait_uncanny.jpg`;
					let imagem = document.createElement("img");
					imagem.src = imagemURL;

					let div = document.createElement("div")
					div.id = "info"
					let paragrafoNome = document.createElement("p");
					let paragrafoDesc = document.createElement("p");
					
					paragrafoNome.innerHTML = Pname;
					paragrafoDesc.innerHTML = Pdesc;

					div.appendChild(paragrafoNome);
					div.appendChild(paragrafoDesc);
					conteudo.appendChild(div);

					conteudo.appendChild(imagem);
				}  

		  		imageURL = `${thumbnail.path}/portrait_xlarge.jpg`;

		  		let image = document.createElement("img");
		  		image.src = imageURL;
		  		image.classList.add("herois");
		  		//document.body.appendChild(image);

		  		a.appendChild(image)

		  		content[0].appendChild(a)

		  	}
		  )
		  .catch((error) => console.log(error))
		});


	// Get the <span> element that closes the modal
	let span = document.getElementsByClassName("close")[0];


	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}